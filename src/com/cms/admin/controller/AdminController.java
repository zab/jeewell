package com.cms.admin.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.portlet.ModelAndView;

import com.cms.admin.service.AdminService;

/**
 * Copyright (C) 2013 2016 Well
 * 文件:AdminController.java
 * 创建:Well
 * 日期:2016年6月28日 上午8:53:30
 * 来自:
 * 版本:
 * 描述:后台管理元的controller类
 */

@Controller
@RequestMapping("/admin")
public class AdminController
{
	@Autowired
	private AdminService adminService;
	
	@RequestMapping(value="/",method={RequestMethod.GET})
	public String gotoAdminRootPage()
	{
		return "admin/module/index";
	}
	
	@RequestMapping(value="/login",method={RequestMethod.GET})
	public ModelAndView login(HttpServletRequest request,ModelMap modelMap)
	{
		String viewName=adminService.login(modelMap);
		return new ModelAndView(viewName, modelMap);
	}
	
	@RequestMapping(value="/logout",method={RequestMethod.POST})
	public ModelAndView logout(HttpServletRequest request,ModelMap modelMap)
	{
		String viewName=adminService.logout(modelMap);
		return new ModelAndView(viewName, modelMap);
	}
	
}
