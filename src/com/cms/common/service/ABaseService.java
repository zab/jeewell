package com.cms.common.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.common.Mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Copyright (C) 1990 2013 Well
 * ABaseService.java
 * 创建人:王奥
 * 日期:2015-10-12 15:11
 * 描述:
 *      service抽象基类
 *      要实现使用该service抽象基类里面的通用方法，需要两个步骤：
 *      1.XXXService类继承该类
 *      2.要新建XXXMapper接口继承tk.mybatis.mapper.common.Mapper接口
 * 历史:
 */

public abstract class ABaseService<T>
{
    @Autowired
    private Mapper<T> mapper;

    /**
     * 查询实体的所有数据，不支持级联查询
     * T 就是当前实体的对象
     * @return T实体对象的所有数据
     */
    public List<T> selectList()
    {
        return mapper.selectAll();
    }

    /**
     * 查询实体列表，不支持级联查询
     *      实体对象存在字段不为null，这些字段的值在sql语句中都是and条件
     *      实体对象为null或者字段全部为null，查询实体所有的数据
     * @param entity 实体对象
     * @return 符合实体字段值的数据
     */
    public List<T> selectList(T entity)
    {
        return mapper.select(entity);
    }

    /**
     * 查询实体
     *      实体对象不能为null，且返回结果必须唯一
     * @param entity 实体对象
     * @return 符合实体字段值的唯一数据
     */
    public T select(T entity)
    {
        return mapper.selectOne(entity);
    }

    /**
     * 根据主键id查询实体
     * @param id 主键
     * @return 符合主键id的唯一数据
     */
    public T select(String id)
    {
        return mapper.selectByPrimaryKey(id);
    }

    /**
     * 分页查询实体列表(集成了mybatis pagehelper插件)，不支持级联查询
     * @param entity 实体对象
     * @param page 当前页码
     * @param rows 每页显示的行数
     * @return 分页的map(rows表示当前页的列表数据，total表示所有数据的条数)
     */
    public Map<String,Object> selecMapByPageHelper(T entity,int page,int rows)
    {
        Map<String,Object> responseMap=new HashMap<String, Object>();

        PageHelper.startPage(page,rows);
        List<T> list= mapper.select(entity);
        responseMap.put("rows",list);
        responseMap.put("total",((Page)list).getTotal());

        return responseMap;
    }

    /**
     * 分页查询实体列表，不支持级联查询
     * @param entity 实体对象
     * @param page 当前页码
     * @param rows 每页显示的行数
     * @return 分页的map(rows表示当前页的列表数据，total表示所有数据的条数)
     */
    public Map<String,Object> selectMapByRowBounds(T entity,int page,int rows)
    {
        Map<String,Object> responseMap=new HashMap<String, Object>();

        List<T> list= mapper.selectByRowBounds(entity, new RowBounds(page, rows));
        responseMap.put("rows",list);
        responseMap.put("total",((Page)list).getTotal());

        return responseMap;
    }

    /**
     * 新增实体
     * @param entity 实体对象
     * @return 新增的个数
     *      1---新增成功
     *      0---新增失败
     */
    public int insert(T entity)
    {
        return mapper.insert(entity);
    }

    /**
     * 新增实体，只新增实体非null字段，不影响有默认值的字段
     * @param entity 实体对象
     * @return 新增的个数
     *      1---新增成功
     *      0---新增失败
     */
    public int insertSelective(T entity)
    {
        return mapper.insertSelective(entity);
    }

    /**
     * 批量新增，支持批量插入的数据库
     * 实体列表中主键必须已经存在值：
     *      主键id采用UUID，推荐使用IDUtil的generate方法生成主键
     * @param entityList
     * @return 新增的个数
     *      >0---新增成功
     *      =0---新增失败
     */
    public int insertList(List<T> entityList)
    {
        return mapper.insertList(entityList);
    }

    /**
     * 批量新增，支持批量插入的数据库，只更新非null字段，不影响数据库中的默认值
     * 实体列表中主键必须已经存在值：
     *      主键id采用UUID，推荐使用IDUtil的generate方法生成主键
     * @param entityList
     * @return 新增的个数
     *      >0---新增成功
     *      =0---新增失败
     */
    public int insertSelectiveList(List<T> entityList)
    {
        return mapper.insertSelectiveList(entityList);
    }

    /**
     * 更新实体，根据主键更新
     * @param entity 实体对象
     * @return 更新的个数
     *      1---更新成功
     *      0---更新失败
     */
    public int update(T entity)
    {
        return mapper.updateByPrimaryKey(entity);
    }

    /**
     * 更新实体，根据主键更新，只更新实体非null字段，不影响有默认值的字段
     * @param entity 实体对象
     * @return 更新的个数
     *      1---更新成功
     *      0---更新失败
     */
    public int updateSelective(T entity)
    {
        return mapper.updateByPrimaryKeySelective(entity);
    }

    /**
     * 批量更新，支持批量更新的数据库
     * 实体列表中的id值必须非null，根据主键进行更新
     * @param entityList
     * @return 更新的个数
     *      >0---更新成功
     *      =0---更新失败
     */
    public int updateList(List<T> entityList)
    {
        return mapper.updateByPrimaryKeyList(entityList);
    }

    /**
     * 批量更新，支持批量更新的数据库，只更新实体非null字段，不影响有默认值的字段
     * 实体列表中的id值必须非null，根据主键进行更新
     * @param entityList
     * @return 更新的个数
     *      >0---更新成功
     *      =0---更新失败
     */
    public int updateSelectiveList(List<T> entityList)
    {
        return mapper.updateByPrimaryKeySelectiveList(entityList);
    }

    /**
     * 删除实体，根据主键id删除
     * @param id 主键
     * @return 删除的个数
     *      1---删除成功
     *      0---删除失败
     */
    public int delete(String id)
    {
        return mapper.deleteByPrimaryKey(id);
    }

    /**
     * @param entity 实体对象
     * @return 删除的个数
     *      大于0---删除成功
     *      0---删除失败
     */
    public int delete(T entity)
    {
        return mapper.delete(entity);
    }

    /**
     * 删除实体列表，根据idList删除
     * @param idList
     * @return 删除的个数
     *      >0---删除成功
     *      =0---删除失败
     */
    public int deleteList(List<String> idList)
    {
        return mapper.deleteByPrimaryKeyList(idList);
    }

}
